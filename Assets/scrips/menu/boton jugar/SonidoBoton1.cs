﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoBoton1 : MonoBehaviour
{
    public AudioSource audioSource2;
    public AudioClip select;
    public AudioClip click;

    // Start is called before the first frame update

    public void selectSound()
    {
        audioSource2.PlayOneShot(select);
    }

    public void clickSound()
    {
        audioSource2.PlayOneShot(click);
    }
}
