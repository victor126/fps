﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncher : FireTemplate
{
    public float throughForce;

    public override void Fire()
    {
        GetComponent<Rigidbody>().AddForce(throughForce * transform.forward, ForceMode.Impulse);
    }

}
